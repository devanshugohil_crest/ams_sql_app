from typing import List
from datetime import datetime
from pydantic import BaseModel

class AssetCreate(BaseModel):
    asset_tag:str
    
    
class AssetBase(AssetCreate):
    id:int
    created_at:datetime
    updated_at:datetime
    employee_code:str
    class Config:
        orm_mode = True
        
        
class EmployeeCreate(BaseModel):
    emp_code:str
    emp_name:str


class EmployeeBase(BaseModel):
    id:int
    emp_code:str
    emp_name:str
    # updated_at: str { format: 'datetime'}

    class Config:
        orm_mode = True






# class ItemBase(BaseModel):
#     title: str
#     description: str = None


# class ItemCreate(ItemBase):
#     pass


# class Item(ItemBase):
#     id: int
#     owner_id: int

#     class Config:
#         orm_mode = True


# class UserBase(BaseModel):
#     email: str


# class UserCreate(UserBase):
#     password: str


# class User(UserBase):
#     id: int
#     is_active: bool
#     items: List[Item] = []

#     class Config:
#         orm_mode = True