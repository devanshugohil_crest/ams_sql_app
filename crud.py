from sqlalchemy.orm import Session

import models, schemas


def get_employee(db: Session, emp_code: int):
    return db.query(models.Employee).filter(models.Employee.emp_code == emp_code).first()


def get_employees(db: Session):
    return db.query(models.Employee).all()


def create_employee(db: Session, emp: schemas.EmployeeCreate):
    db_employee = models.Employee(emp_code = emp.emp_code,emp_name = emp.emp_name)
    db.add(db_employee)
    db.commit()
    db.refresh(db_employee)
    return db_employee


def get_assets(db: Session,):
    return db.query(models.Asset).all()


def create_emp_asset(db: Session, asset: schemas.AssetBase, emp_code: int):
    db_asset = models.Asset(**asset.dict(), employee_code=emp_code)
    db.add(db_asset)
    db.commit()
    db.refresh(db_asset)
    return db_asset