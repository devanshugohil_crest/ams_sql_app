from typing import List
import time
from starlette.requests import Request
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
import crud, models, schemas
from database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    print("time taken for response:",str(process_time))
    response.headers["X-Process-Time"] = str(process_time)
    return response
# Dependency
def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@app.post("/employee/", response_model=schemas.EmployeeBase)
def create_employee(emp: schemas.EmployeeCreate, db: Session = Depends(get_db)):
    db_emp = crud.get_employee(db, emp_code = emp.emp_code)
    if db_emp:
        raise HTTPException(status_code=400, detail={ 'emp_code': "Employee already registered"})
    db_employee = models.Employee(emp_code = emp.emp_code,emp_name = emp.emp_name)
    db.add(db_employee)
    db.commit()
    db.refresh(db_employee)
    return db_employee


@app.get("/employees/", response_model=List[schemas.EmployeeBase])
def read_employees(db: Session = Depends(get_db)):
    employees = crud.get_employees(db)
    return employees


@app.get("/employee/{emp_code}", response_model=schemas.EmployeeBase)
def read_user(emp_code: int, db: Session = Depends(get_db)):
    db_employee = crud.get_employee(db, emp_code=emp_code)
    if db_employee is None:
        raise HTTPException(status_code=404, detail="employees not found")
    return db_employee


@app.post("/employees/{emp_code}/assets/", response_model=schemas.AssetBase)
def create_item_for_user(emp_code: int, asset: schemas.AssetCreate, db: Session = Depends(get_db)):
    return crud.create_emp_asset(db=db, asset=asset, emp_code=emp_code)


@app.get("/assets/", response_model=List[schemas.AssetBase])
def read_assets(db: Session = Depends(get_db)):
    items = crud.get_assets(db)
    return items